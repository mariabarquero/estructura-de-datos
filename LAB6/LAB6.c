//María José Barquero Pérez/2019037947
#include<stdio.h>
#include<stdlib.h>
#define True 1
#define False 0

struct nodo {
    int numero;
    struct nodo *izq;   //puntero de subarbol Izquierdo
    struct nodo *der;   //puntero de subarbol Derecho
};

struct nodo *raiz=NULL;      //Se crea un puntero Raiz

void insertar_numero(int numero){
    
    struct nodo *nuevo;                   //Se crea un nuevo puntero
    nuevo = malloc(sizeof(struct nodo));  //Se reserva memoria
    nuevo->numero = numero;               //Se asigna el valor ingresado al nodo numero
    nuevo->izq = NULL;                    //Los punteros se asigna en NULL
    nuevo->der = NULL;
    
    if (raiz == NULL){                  //Si el arbol no tiene subarboles, entonces raiz pasa a ser el nuevo numero
        raiz = nuevo;
        
    }else{                             //Si el arbol no esta vacio entonces se agrega numero
        struct nodo *puntero_ant, *temporal;
        puntero_ant = NULL;
        temporal = raiz;
        while (temporal != NULL)      // Mientras Temporal sea diferente de NULL
        {
            puntero_ant = temporal;   //el puntero anterior pasa a ser temporal(la raiz)
            if (numero < temporal->numero) //Se hace la validacion, si el numero es menor al numero del arbol mover a la izquierda
                temporal = temporal->izq;
            else                      //Si es mayor mover a la derecha
                temporal = temporal->der;
        }
        if (numero < puntero_ant->numero)   //Si el numero anterior es mayor  anterior izquierdo pasa a ser el nuevo numero
            puntero_ant->izq = nuevo;
        else                               //Si es menor anterior izquierdo pasar a ser el nuevo numero 
            puntero_ant->der = nuevo;
    }
}


void imprimirPostOrden(struct nodo *temporal)
{
    if (temporal != NULL)
    {
        imprimirPostOrden(temporal->izq);
        imprimirPostOrden(temporal->der);
        printf("%i\n",temporal->numero);
    }
}


int main(){
    
    insertar_numero(8);
    insertar_numero(10);
    insertar_numero(3);
    insertar_numero(6);
    insertar_numero(1);
    insertar_numero(4);
    insertar_numero(7);
    insertar_numero(14);
    insertar_numero(13);

    
    printf("Impresion postorden: \n");
    imprimirPostOrden(raiz);

    
    return True;
}

