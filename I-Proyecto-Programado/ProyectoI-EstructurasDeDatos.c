/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    I Proyecto 
    
    Estudiantes:Raychel Arguedas Bolívar 
                María José Barquero Pérez 
                Te Chen Huang

**********************************************************************/
//Bibliotecas utilizadas
#include <stdlib.h>
#include <stdio.h>
#include <time.h>


typedef struct nodo{                     //Se crea un struct de tipo nodo 
	char *nombre_carta;                  //Ese nodo va a contener un nombre de carta,valor de la carta,valor de familia
    int valor_carta;             
	int valor_familia;
    int contador;
 	struct nodo *puntero_siguiente;      //Va a tener dos punteros siguiente y anterior
	struct nodo *puntero_anterior;
}nodo;

nodo *head = NULL;                       //Se crea head, para referenciar a la primera carta siempre
nodo *inicio = NULL;                     //Puntero inicio que nos va a indicar el primero de la lista
nodo *final = NULL;                      //Puntero final, que nos indica el ultimo de la lista 


void agregar_nodo();
void imprimir_inicio_final();


/*-------------------------------------------------------------------------------------------
	Nombre de la función: recorrer_lista
	Entradas: Recibe un puntero de tipo nodo starty un int indicador
	Salidas: Retorna un puntero de tipo nodo start
	Funcionamiento: 
        -Esta función sirve para recorrer la lista y encontrar el último elemento 
        -de la lista
        -Es un ciclo, el indicador se decrementa 1 cada vez y start apunta puntero
        -siguiente
		-Retorna el puntero de tipo nodo start.
-------------------------------------------------------------------------------------------*/
struct nodo *recorrer_lista(nodo *start, int indicador) {
    while (indicador != 0){ 
        start = start->puntero_siguiente; 
        indicador--; 
    }
    return start; 
}

/*-------------------------------------------------------------------------------------------
	Nombre de la función: agregar_nodo
	Entradas: Recibe 4 variables (nombre_carta,valor_carta,valor_familia,contador)
	Salidas: No retorna nada
	Funcionamiento: 
        -Se puede usar esta función para crear un nuevo nodo
        -Primero se debe solicitar espacio en memoria
        -Si inicio es igual a NULL:
            *Asigna los valores (nombre_carta,valor_carta,contador,valor_familia)a 
            los datos del nodo
            *Inicio va ser igual que el nuevo nodo agregado
            *Anterior ahora es NULL 
            *Siguiente también es NULL
            *En este caso inicio va ser igual que el final
            *Head va a ser inicio osea el nuevo nodo en este caso
        -Si inicio no es igual a NULL;
            *Asigna los valores (nombre_carta,valor_carta,contador,valor_familia)
            a los datos del nodo
            *EL siguiente de final va a ser el nuevo nodo
            *El siguiente del nuevo nodo ahora es NULL
            *El anterior del nuevo nodo ahora es final
            *Final ahora va a ser en el nuevo nodo
            *Head siempre va a ser el primero 
-------------------------------------------------------------------------------------------*/
void agregar_nodo(char *nombre_carta, int valor_carta,int valor_familia,int contador){    
	nodo *nuevo_nodo = (nodo *)malloc(sizeof(nodo));       
	if(inicio == NULL){                                    
		nuevo_nodo->nombre_carta = nombre_carta;           
		nuevo_nodo->valor_carta = valor_carta;
		nuevo_nodo->valor_familia = valor_familia;
        nuevo_nodo->contador = contador;
		inicio = nuevo_nodo;                               
		inicio->puntero_siguiente = NULL;                 
		inicio->puntero_anterior = NULL;                   
		final = inicio;                                    
        head = inicio;                                  
	}else{
		nuevo_nodo->nombre_carta = nombre_carta;           
		nuevo_nodo->valor_carta = valor_carta;
		nuevo_nodo->valor_familia = valor_familia;
        nuevo_nodo->contador = contador;
		final->puntero_siguiente = nuevo_nodo;            
		nuevo_nodo->puntero_siguiente = NULL;             
		nuevo_nodo->puntero_anterior = final;              
		final = nuevo_nodo;                               
        head = inicio;                                    
    }
}

/*-----------------------------------------------------------------------
	Nombre de la función: print_red
	Entradas: No recibe nada
	Salidas: No recibe nada
	Funcionamiento: 
        -Configurar el texto al color rojo
-----------------------------------------------------------------------*/
void print_red(){
    printf("\033[1;31m");  
}

/*-----------------------------------------------------------------------
	Nombre de la función: print_yellow
	Entradas: No recibe nada
	Salidas: No recibe nada
	Funcionamiento: 
        -Configurar el texto al color amarrillo
-----------------------------------------------------------------------*/
void print_yellow(){
    printf("\033[1;33m");  
}

/*-----------------------------------------------------------------------
	Nombre de la función: print_green
	Entradas: No recibe nada
	Salidas: No recibe nada
	Funcionamiento: 
        -Configurar el texto al color verde
-----------------------------------------------------------------------*/
void print_green(){
    printf("\033[1;32m");  
}

/*-----------------------------------------------------------------------
	Nombre de la función: print_blue
	Entradas: No recibe nada
	Salidas: No recibe nada
	Funcionamiento: 
        -Configurar el texto al color azul
-----------------------------------------------------------------------*/
void print_blue(){
    printf("\033[1;34m");  
}

/*-----------------------------------------------------------------------
	Nombre de la función: print_cyan
	Entradas: No recibe nada
	Salidas: No recibe nada
	Funcionamiento: 
        -Configurar el texto al color celeste
-----------------------------------------------------------------------*/
void print_cyan(){
    printf("\033[0;36m");  
}

/*-------------------------------------------------------------------------------------------
	Nombre de la función: representar_cartas
	Entradas: Recibe tres enteros (numero_carta, numero_familia, contador_generador)
	Salidas: No retorna nada
	Funcionamiento: 
        -Esta función sirve para representar cartas en consola
        -Codigo UTF para representar simbolos de cartas
            *"\u2660" representa el símbolo Espada en UTF
            *"\u2665" representa el símbolo Corazón en UTF
            *"\u2666" representa el símbolo Diamante en UTF
            *"\u2663" representa el símbolo Trébol en UTF
        -Cuando el numero_familia es 0, imprime las cartas de espadas
        -Cuando el numero_familia es 13, imprime las cartas de corazón
        -Cuando el numero_familia es 26, imprime las cartas de diamantes
        -Cuando el numero_familia es 39, imprime las cartas de Tréboles      
-------------------------------------------------------------------------------------------*/
void representar_cartas(int numero_carta,int numero_familia,int contador_general){
    char *Espada = "\u2660"; //Espada
    char *Corazon = "\u2665"; //Corazón
    char *Diamante = "\u2666"; //Diamante
    char *Trebol = "\u2663"; //Trébol
    if (numero_familia == 0){   
			   if (numero_carta <=9){
                   if (numero_carta == 1){
                       print_red(); 
                       //Cuando la carta sea 1
			           printf("┌──────────┐\n");
                       printf("│A         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Espada);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        A │\n");
                       printf("└──────────┘\n");
                   }else{
                     print_red(); 
                     //Cuando el número de carta sea menor o igual a 9
			         printf("┌──────────┐\n");
                     printf("│%d         │\n",numero_carta);  
                     printf("│          │\n");
                     printf("│          │\n");
                     printf("│    %s     │\n",Espada);
                     printf("│          │\n");
                     printf("│          │\n");
                     printf("│        %d │\n",numero_carta);
                     printf("└──────────┘\n");
                    }
			   }else{
                    if (numero_carta == 11){
                       print_red(); 
                       //Cuando la carta sea 1
			           printf("┌──────────┐\n");
                       printf("│J         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Espada);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        J │\n");
                       printf("└──────────┘\n");
                    }else{
                       if (numero_carta == 12){
                          print_red(); 
                          //Cuando la carta sea 12
			              printf("┌──────────┐\n");
                          printf("│Q         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Espada);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        Q │\n");
                          printf("└──────────┘\n");
                        }else{
                          if (numero_carta == 13){
                          print_red(); 
                          //Cuando la carta sea 13
			              printf("┌──────────┐\n");
                          printf("│K         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Espada);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        K │\n");
                          printf("└──────────┘\n");
                    }else{
                       print_red(); 
                       printf("┌──────────┐\n");
                       printf("│%d        │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Espada);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│       %d │\n",numero_carta);
                       printf("└──────────┘\n");
                    }
                }
            }
		}
    }

			if (numero_familia == 13){         
				if (numero_carta <=9){
                    if (numero_carta == 1){
                       print_yellow();
                       //Cuando la carta sea 1
			           printf("┌──────────┐\n");
                       printf("│A         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Corazon);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        A │\n");
                       printf("└──────────┘\n");
                   }else{
                       print_yellow();
			           printf("┌──────────┐\n");
                       printf("│%d         │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Corazon);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        %d │\n",numero_carta);
                       printf("└──────────┘\n");
                   }
				}else{
                      if (numero_carta == 11){
                       print_yellow(); 
                       //Cuando la carta sea 11
			           printf("┌──────────┐\n");
                       printf("│J         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Corazon);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        J │\n");
                       printf("└──────────┘\n");
                    }else{
                       if (numero_carta == 12){
                          print_yellow(); 
                          //Cuando la carta sea 12
			              printf("┌──────────┐\n");
                          printf("│Q         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Corazon);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        Q │\n");
                          printf("└──────────┘\n");
                        }else{
                          if (numero_carta == 13){
                          print_yellow(); 
                          //Cuando la carta sea 13
			              printf("┌──────────┐\n");
                          printf("│K         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Corazon);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        K │\n");
                          printf("└──────────┘\n");
                    }else{
                      print_yellow();
				      printf("┌──────────┐\n");
                      printf("│%d        │\n",numero_carta);  
                      printf("│          │\n");
                      printf("│          │\n");
                      printf("│    %s     │\n",Corazon);
                      printf("│          │\n");
                      printf("│          │\n");
                      printf("│       %d │\n",numero_carta);
                      printf("└──────────┘\n");
                    }
			    } 
            }       
		}
	}

			if (numero_familia == 26){
				if (numero_carta <=9){
                    if (numero_carta == 1){
                       print_green();
                       //Cuando la carta sea 1
			           printf("┌──────────┐\n");
                       printf("│A         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Diamante);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        A │\n");
                       printf("└──────────┘\n");
                   }else{
                       print_green();
			           printf("┌──────────┐\n");
                       printf("│%d         │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Diamante);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        %d │\n",numero_carta);
                       printf("└──────────┘\n");
                   }
				}else{
                   
                  if (numero_carta == 11){
                       print_green(); 
                       //Cuando la carta sea 11
			           printf("┌──────────┐\n");
                       printf("│J         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Diamante);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        J │\n");
                       printf("└──────────┘\n");
                    }else{
                       if (numero_carta == 12){
                          print_green(); 
                          //Cuando la carta sea 12
			              printf("┌──────────┐\n");
                          printf("│Q         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Diamante);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        Q │\n");
                          printf("└──────────┘\n");
                        }else{
                          if (numero_carta == 13){
                          print_green(); 
                          //Cuando la carta sea 13
			              printf("┌──────────┐\n");
                          printf("│K         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Diamante);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        K │\n");
                          printf("└──────────┘\n");
                    }else{
                       print_green(); 
                       printf("┌──────────┐\n");
                       printf("│%d        │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Diamante);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│       %d │\n",numero_carta);
                       printf("└──────────┘\n");
                    }
                  }
                }
			}
		}

			if (numero_familia == 39){
                if (numero_carta <=9){
                    if (numero_carta == 1){
                       print_blue();
                       //Cuando la carta sea 1
			           printf("┌──────────┐\n");
                       printf("│A         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Trebol);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        A │\n");
                       printf("└──────────┘\n");
                   }else{
                       print_blue(); 
			           printf("┌──────────┐\n");
                       printf("│%d         │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Trebol);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        %d │\n",numero_carta);
                       printf("└──────────┘\n");
                   }
				}else{
                   if (numero_carta == 11){
                       print_blue(); 
                       //Cuando la carta sea 11
			           printf("┌──────────┐\n");
                       printf("│J         │\n");  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Trebol);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│        J │\n");
                       printf("└──────────┘\n");
                    }else{
                       if (numero_carta == 12){
                          print_blue(); 
                          //Cuando la carta sea 12
			              printf("┌──────────┐\n");
                          printf("│Q         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Trebol);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        Q │\n");
                          printf("└──────────┘\n");
                        }else{
                          if (numero_carta == 13){
                          print_blue(); 
                          //Cuando la carta sea 13
			              printf("┌──────────┐\n");
                          printf("│K         │\n");  
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│    %s     │\n",Trebol);
                          printf("│          │\n");
                          printf("│          │\n");
                          printf("│        K │\n");
                          printf("└──────────┘\n");
                    }else{
                       print_blue(); 
                       printf("┌──────────┐\n");
                       printf("│%d        │\n",numero_carta);  
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│    %s     │\n",Trebol);
                       printf("│          │\n");
                       printf("│          │\n");
                       printf("│       %d │\n",numero_carta);
                       printf("└──────────┘\n");
                    }
                } 
            }
		}
	}
}


/*-------------------------------------------------------------------------------------------
	Nombre de la función: imprimir_inicio_final
	Entradas: No recibe nada
	Salidas: No retorna nada
	Funcionamiento: 
        -Primero se debe solicitar espacio en memoria
        -Actual va ser igual a la primera carta
        -Se crean tres variables temporales: numero_carta empieza en 1, 
        numero_familia empieza en 0, contador general empieza en 1
        -Si la lista se encuentre vacía
            *Mientras actual sea distinto de NUll
            *Se asignan el valor de carta, familia y contador a las variables temporales
            *Se llama a la funcion para representar forma de carta
            *Se recorre la lista 
        -Si la lista está vacía, imprime "La lista se encuentra vacia"
-------------------------------------------------------------------------------------------*/
void imprimir_inicio_final(){
	nodo *actual = (nodo *)malloc(sizeof(nodo));
	actual = head;                                     
    int numero_carta=1,numero_familia=0,contador_general=1;
	
	if(inicio!=NULL){                                   
		while(actual != NULL){                        
			numero_carta = actual->valor_carta;        
			numero_familia = actual->valor_familia;
            contador_general=actual->contador;
            representar_cartas(numero_carta,numero_familia,contador_general);       
			actual = actual->puntero_siguiente;		              
		}
	}else{
		printf("\n La lista se encuentra vacia\n\n");
	}
}

/*-------------------------------------------------------------------------------------------
	Nombre de la función: imprimir_actual
	Entradas: No recibe nada
	Salidas: No retorna nada
	Funcionamiento: 
        -Primero se debe solicitar espacio en memoria
        -Actual va ser igual que el inicio
        -Se crean tres variables temporales: numero_carta empieza en 1, 
        numero_familia empieza en 0, contador general empieza en 1
        -Se asignan el valor de carta, familia y contador a las variables temporales
        -Se llama a la funcion para representar forma de carta
-------------------------------------------------------------------------------------------*/
void imprimir_actual(){
    nodo *actual = (nodo *)malloc(sizeof(nodo));
	actual = inicio;                                              
    int numero_carta=1,numero_familia=0,contador_general=1;                 
    numero_carta = actual->valor_carta; 
	numero_familia = actual->valor_familia;
    contador_general=actual->contador;    
    representar_cartas(numero_carta,numero_familia,contador_general);
}

/*------------------------------------------------------------------------------------------
	Nombre de la función: imprimir_siguiente_carta
	Entradas: No recibe nada
	Salidas: No retorna nada
	Funcionamiento:
        -La carta a mostrar es la carta siguiente de la actual 
        -Primero se debe solicitar espacio en memoria
        -Actual va ser igual que el inicio
        -Si la carta siguiente de la actual es diferente de NULL
            *La carta actual ahora va a ser la carta siguiente de la actual 
            *Inicio ahora es la carta actual
            *Se crean tres variables temporales: numero_carta empieza en 1, 
            numero_familia empieza en 0, contador general empieza en 1
            *Se asignan el valor de carta, familia y contador a las variables temporales
            *Se llama a la funcion para representar forma de carta
        -Si la carta siguiente de la actual es NULL,significa que ha llegado 
        al final de la lista imprime
                        "Ha llegado al final de la baraja"
------------------------------------------------------------------------------------------*/
void imprimir_siguiente_carta(){                        
    nodo *actual = (nodo *)malloc(sizeof(nodo));
    actual = inicio;                                    

    if(actual->puntero_siguiente != NULL){              
        actual=inicio->puntero_siguiente;              
        
        inicio=actual;                                     

        int numero_carta=1,numero_familia=0,contador_general=1;
        numero_carta = actual->valor_carta;
	    numero_familia = actual->valor_familia;
        contador_general=actual->contador;   
        representar_cartas(numero_carta,numero_familia,contador_general);
        
    }else{
        printf("Ha llegado al final de la baraja");    
    }
}

/*------------------------------------------------------------------------------------------
	Nombre de la función: imprimir_anterior
	Entradas: No recibe nada
	Salidas: No retorna nada
	Funcionamiento:
        -La carta a mostrar es la carta anterior de la actual 
        -Primero se debe solicitar espacio en memoria
        -Actual va ser igual que el inicio
        -Si anterior de la carta actual es diferente de NUll
            *La carta actual ahora es la carta anterior de actual 
            *Inicio va a ser la nueva actual 
            *Se crean tres variables temporales: numero_carta empieza en 1, 
            numero_familia empieza en 0, contador general empieza en 1
            *Se asignan el valor de carta, familia y contador a las variables temporales
            *Se llama a la funcion para representar forma de carta
        -Si anterior de la carta actual es igual a NULL,significa que está 
        al inicio de la lista
                        "Su carta actual es la primera de la baraja."
------------------------------------------------------------------------------------------*/
void imprimir_anterior(){
    nodo *actual = (nodo *)malloc(sizeof(nodo));
    actual= inicio;                                     
    if(actual->puntero_anterior != NULL){             
        actual=inicio->puntero_anterior;               
        
        inicio=actual;                                 

        int numero_carta=1,numero_familia=0,contador_general=1;
        numero_carta = actual->valor_carta;
	    numero_familia = actual->valor_familia;
        contador_general=actual->contador;       
        representar_cartas(numero_carta,numero_familia,contador_general);
    }else{                                             
        printf("Su carta actual es la primera de la baraja.");
    }
}

/*------------------------------------------------------------------------------------------
	Nombre de la función: swap_number
	Entradas: recibe dos enteros
	Salidas: No retorna nada
	Funcionamiento:
        -En esta función,se crea un temporal y hace un swap de enteros.
------------------------------------------------------------------------------------------*/
void swap( int *primer_numero, int *segundo_numero ){   
    int temporal = *primer_numero;      
    *primer_numero = *segundo_numero ;       
    *segundo_numero = temporal;   
}

/*------------------------------------------------------------------------------------------
	Nombre de la función: swap_char
	Entradas: recibe dos valores a los que apuntan un par de char*
	Salidas: No retorna nada
	Funcionamiento:
        -En esta función,se crea un temporal y hace un swap de char*.
------------------------------------------------------------------------------------------*/
 void swap_char(char * *primer_char, char * *segundo_char){ 
  char *temp = *primer_char; 
  *primer_char= *segundo_char; 
  *segundo_char = temp; 
}   


/*------------------------------------------------------------------------------------------
	Nombre de la función: particion
	Entradas: Recibe dos punteros de tipo nodo inicial y final
	Salidas: Retorna el puntero de tipo nodo i
	Funcionamiento:
        -Esta función sirve para dividir la lista en dos, los menores al lado izquierdo
        y los mayores al lado derecho   
        -(int x  = final->contador) se define el último elemento de la 
        lista como pivote
        -(nodo *i = inicial->puntero_anterior)se define un puntero i que apunta a la 
        carta anterior de la carta inicial
        -Un ciclo, (for (nodo *j = inicial; j != final; j = j->puntero_siguiente))Asigna e inicializa
        un puntero al inicio, lo compara con el elemento final y lo pasa al siguiente usando 
        puntero_siguiente, es similiar a j++
        -Si el valor es menor que el pivote lo pasa al lado izquierdo:
                i = (i == NULL)? inicial : i->puntero_siguiente;
        Es un operador condicional ? en C, tiene tres operandos, también se conoce como operador
        ternario. Primero se evaluá la expresión i = (i == NULL)?, si el resultado de la 
        evaluación es verdadero(diferente de cero), se ejecuta la expresión inicial y si es 
        falso se ejecuta la expresión i->puntero_siguiente
            *"i->puntero_siguiente" funciona como i++
            *Hacemos swap de valores y char (no de nodo), es necesario hacer swap con todos los 
            datos que contiene un nodo
        -Si el valor es menor que el pivote,lo pasa al lado derecho y también hay que hacer
        swap.
------------------------------------------------------------------------------------------*/
nodo *particion(nodo *initiation, nodo *final){
    int x  = final->contador;
 
    nodo *i = initiation->puntero_anterior;

    for (nodo *j = initiation; j != final; j = j->puntero_siguiente){
        if (j->contador <= x){
            i = (i == NULL)? initiation : i->puntero_siguiente;
            swap_char(&(i->nombre_carta),&(j->nombre_carta));
            swap(&(i->valor_familia),&(j->valor_familia));  
            swap(&(i->valor_carta), &(j->valor_carta));      
            swap(&(i->contador), &(j->contador));    
        }
    }
            i = (i == NULL)? initiation : i->puntero_siguiente; 
            swap_char(&(i->nombre_carta),&(final->nombre_carta));
            swap(&(i->valor_familia),&(final->valor_familia));
            swap(&(i->valor_carta), &(final->valor_carta)); 
            swap(&(i->contador), &(final->contador)); 
            return i;
}


/*------------------------------------------------------------------------------------------
	Nombre de la función: quick_sort
	Entradas: Recibe dos punteros de tipo nodo inicial y final
	Salidas: No retorna nada
	Funcionamiento:
        -Esta función sirve para ordenar una lista llamando la función particion para dividir
        la lista en dos, los que son menores que el pivote se pone en el lado izquierdo y los
        que son mayores se ponen en el lado derecho.
        -Si el final es diferente que nulo, inicio no es igual que final, y inicio no es 
        igual que final->puntero_siguiente:
                *Se define un nuevo struct que se va a llamar mitad
                *(struct nodo *mitad = particion(inicial, final)) el pivote en la primera 
                vuelta va ser el último de la lista
                *Vuelve a llamar(quick_sort(inicial, mitad->puntero_anterior))  y esta vez 
                el pivote va a ser mitad->puntero_anterior, y va verificar si los elementos 
                que está en el lado izquiedo ya está en orden
                *Al final llama quick_sort(mitad->puntero_siguiente, final))  y esta vez 
                el pivote va a ser mitad->puntero_siguiente, y va verificar si los elementos 
                que está en el lado derecho ya está en orden
------------------------------------------------------------------------------------------*/
void quick_sort(struct nodo *inicial, struct nodo *final){
    if (final != NULL && inicial != final && inicial != final->puntero_siguiente){
        struct nodo *mitad = particion(inicial, final);
        quick_sort(inicial, mitad->puntero_anterior); //sub-lista izquierda
        quick_sort(mitad->puntero_siguiente, final);//sub-lista derecha
    }
}


/*------------------------------------------------------------------------------------------
	Nombre de la función: barajar
	Entradas: Recibe dos punteros de tipo nodo list
	Salidas: No retorna nada
	Funcionamiento:
        -Esta función sirve para barajar una lista ordenada
        -(srand(time(NULL))) Instrucción que inicializa el generador de números aleatorios
        -int variable = rand () % (1000-100+1) + 100; Obtención de un número aleatorio 
        entero entre cero y un valor muy grande
        -Mientras variable no sea 0:
            *Se crean dos variables temporales enteros, son como índices aleatorios
            que se van a cambiar con el swap
            *Se crean dos nodos temporales, se usa uno para buscar la primera variable 
            temporal first_index, y la segunda igual. En este caso usamos la función 
            recorrer_lista para recorrer toda la lista
            *Y después se hacen swaps de valor_carta, nombre_carta, contador,y valor_familia
------------------------------------------------------------------------------------------*/
void barajar(nodo *list){ 
    srand(time(NULL));
    int variable = rand () % (1000-100+1) + 100; 
    while(variable!= 0){
        int first_index = rand()%(52);
        int second_index = rand()%(52);
        nodo *first_card = recorrer_lista(list,first_index);
        nodo *second_card = recorrer_lista(list,second_index);
        swap(&(first_card->valor_carta), &(second_card->valor_carta));
        swap_char(&(first_card->nombre_carta),&(second_card->nombre_carta));
        swap(&(first_card->contador), &(second_card->contador)); 
        swap(&(first_card->valor_familia),&(second_card->valor_familia));
        variable = variable-1;
  }

}

/*------------------------------------------------------------------------------------------
	Nombre de la función: main
	Entradas: No recibe parámetros
	Salidas: return 0
	Funcionamiento: 
		-Esta es la función principal
		-Se crea variables globales opcion, indice,contador_espada,contador_corazon,
        contador_diamante y contador_trebol
        -Primero se debe solicitar espacio en memoria usando malloc
        -La carta actual va a ser igual que la carta head
        -Se usa 4 while para crear las 52 cartas, con su diferente familia  
		-Imprime el menú
		-Utiliza un switch para realizar cada instrucción
				*Opción 1: Mostrar carta actual
				*Opción 2: Mostrar carta siguiente
				*Opción 3: Mostrar carta anterior
				*Opción 4: Mostrar toda la baraja
				*Opción 5: Ordenar(quick_sort)
                *Opción 6: Barajar
                *Opción 7: Salir
------------------------------------------------------------------------------------------*/
int main(){
    int opcion;
	int contador_espada=1,contador_corazon = 1,contador_diamante=1,contador_trebol=1;
    int indice=1;
	nodo *actual = (nodo *)malloc(sizeof(nodo));
	actual = head;    
	while(contador_espada < 14){ 
        agregar_nodo("Espada",contador_espada,0,indice);
        contador_espada++;
        indice++;
    }
    while(contador_corazon < 14){
        agregar_nodo("Corazon",contador_corazon,13,indice);
        contador_corazon++;
        indice++;
    }
       
    while(contador_diamante < 14){
        agregar_nodo("Diamante",contador_diamante,26,indice);
        contador_diamante++;
        indice++;
    }
    
    while(contador_trebol < 14 ){
        agregar_nodo("Trebol",contador_trebol,39,indice);
        contador_trebol++;
        indice++;
    }
	do{
        print_cyan();
        printf("\n**********************************************************************");
		printf("\n1. Mostrar carta actual");
		printf("\n2. Mostrar carta siguiente");
		printf("\n3. Mostrar carta anterior");
		printf("\n4. Mostrar toda la baraja");
		printf("\n5. Ordenar");
        printf("\n6. Barajar");
        printf("\n7. Salir");
        printf("\n**********************************************************************");
		printf("\n>> Opcion deseada: ");
        scanf("%d",&opcion);
        switch(opcion){
            case 1:
                    imprimir_actual();
                    break;
            case 2: 
                    imprimir_siguiente_carta();
                    break;
            case 3:
                    imprimir_anterior();
                    break;
            case 4:
                	printf("\n");
                	imprimir_inicio_final();
                    break;
            case 5:
                    quick_sort(head,final);
                    imprimir_inicio_final();
                    break;
            case 6:
                    barajar(head);
                    imprimir_inicio_final();
                    break;
        }
    }while(opcion != 7);
    //Salir del programa
        return 0;
}

/*------------------------------------------------------------------------------------------

Referencia
Anónimo.(2016). Adding Color to Your Output From C. Recuperado de  
http://web.theurbanpenguin.com/adding-color-to-your-output-from-c/
Anónimo.(s.f.). Generar números o secuencias aleatorios en C. Recuperado de
https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=917:generar-
numeros-o-secuencias-aleatorios-en-c-intervalos-srand-y-rand-time-null-randmax-cu00525f&catid=
82&Itemid=210
Anónimo.(s.f.). Quicksort on Doubly Linked List. Recuperado de 
https://www.geeksforgeeks.org/quicksort-for-linked-list/
Anónimo.(2016).Estructura de datos en C - Lista Doble. Recuperado de
https://www.youtube.com/watch?v=-OMU204Q3lc&t=653s

------------------------------------------------------------------------------------------*/