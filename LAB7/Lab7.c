/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Programación Orientada a Objetos
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
  		

    En este laboratorio, cada número representa un estudiante. (0-24)
    La cantidad total de estudiantes es: 25


		        Laboratorio#7
    
    Estudiantes:María José Barquero Pérez 
                Te Chen Huang

**********************************************************************/

#include <stdio.h> 
#include <stdlib.h> 
  
// Una estructura para representar un nodo de lista de adyacencia
struct Nodo { 
    int Destino; 
    struct Nodo *siguiente;
    } ; 
  
// Una estructura para representar una lista de adyacencia
struct Lista 
{ 
    struct Nodo *head;  
}; 
  
// Una estructura para representar un grafo.
// El Tamano de la matriz será Tamano (número de vértices en el gráfico)
struct Grafo 
{ 
    int Tamano; 
    struct Lista *array; 
}; 
  
// Una función de utilidad para crear un nuevo nodo de lista de adyacencia
struct Nodo *NuevoNodo(int Destino) 
{ 
    struct Nodo  *nuevoNodo = (struct Nodo*) malloc(sizeof(struct Nodo)); 
    nuevoNodo->Destino = Destino; 
    nuevoNodo->siguiente = NULL; 
    return nuevoNodo; 
} 
  
// Una función de utilidad que crea un grafo de vértices 
struct Grafo* CrearGrafo(int Tamano) 
{ 
    struct Grafo *Grafo =  (struct Grafo*) malloc(sizeof(struct Grafo)); 
    Grafo->Tamano = Tamano; 
  
// Crear una matriz de listas de adyacencia. Tamano de matriz será V
    Grafo->array =  (struct Lista*) malloc(Tamano* sizeof(struct Lista)); 
  
// Inicializa cada lista de adyacencia como vacía,haciendo la cabeza como NULL
    int i; 
    for (i = 0; i < Tamano; ++i) 
        Grafo->array[i].head = NULL; 
  
    return Grafo; 
} 
  
// Agrega un borde a un grafo no dirigido
void Agregar(struct Grafo *Grafo, int Inicio, int Destino) 
{ 
// Agrega un borde de Inicio a Destino. Un nuevo nodo es agregado a la lista de adyacencia de Inicio. 
//El nodo se agrega al principio
    struct Nodo *Nuevo = NuevoNodo(Destino); 
    Nuevo->siguiente = Grafo->array[Inicio].head; 
    Grafo->array[Inicio].head = Nuevo; 
  
// Como el gráfico no está dirigido, agregue un borde de Destino a Inicio también
    Nuevo = NuevoNodo(Inicio); 
    Nuevo->siguiente = Grafo->array[Destino].head; 
    Grafo->array[Destino].head = Nuevo; 
} 
  
// Una función de utilidad para imprimir la lista de adyacencia
// representación del grafo
void ImprimirGrafo(struct Grafo* Grafo) 
{ 
    int vertice; 
    for (vertice = 0; vertice < Grafo->Tamano; ++vertice) 
    { 
        struct Nodo *Graph = Grafo->array[vertice].head; 
        printf("\n Adjacency list of vertex %d\n head ", vertice); 
        while (Graph) 
        { 
            printf("-> %d", Graph->Destino); 
            Graph = Graph->siguiente; 
        } 
        printf("\n"); 
    } 
} 
  
// Programa de controlador para probar las funciones anteriores
int main() 
{ 
// crea el grafo dado en la figura de arriba
    int Vertice = 25; 
    struct Grafo* Grafo = CrearGrafo(Vertice); 
    Agregar(Grafo, 0, 0);
    Agregar(Grafo, 1, 1); 
    Agregar(Grafo, 2, 7); 
    Agregar(Grafo, 2, 10); 
    Agregar(Grafo, 2, 14); 
    Agregar(Grafo, 2, 16); 
    Agregar(Grafo, 5, 5); 
    Agregar(Grafo, 6, 6);
    Agregar(Grafo, 19, 13); 
    Agregar(Grafo, 16, 11); 
    Agregar(Grafo, 10, 21); 
    Agregar(Grafo, 15, 17); 
    Agregar(Grafo, 4, 9); 
    Agregar(Grafo, 4, 12); 
    Agregar(Grafo, 7, 11);
    Agregar(Grafo, 7, 24); 
    Agregar(Grafo, 7, 23); 
    Agregar(Grafo, 8, 9); 
    Agregar(Grafo, 8, 11); 
    Agregar(Grafo, 24, 17); 
    Agregar(Grafo, 24, 9); 
    Agregar(Grafo, 24, 22); 
    Agregar(Grafo, 24, 9); 
  // imprime la representación de la lista de adyacencia del grafo anterior
    ImprimirGrafo(Grafo); 
  
    return 0; 
}