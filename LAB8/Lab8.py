""" Instituto Tecnológico de Costa Rica
    Estructura de Datos
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
  	Laboratorio#8
    Estudiante: Maria Jose Barquero Perez 
    Carnet: 2019037947
    Link en Repl.it: https://repl.it/@mariajose8/listas    
"""

class Nodo:
    def __init__(self,valor):
        self.valor=valor
        self.siguiente=None

    def setvalor(self,valor):
        self.valor = valor

    def setSiguiente(self,siguiente):
       self.siguiente = siguiente        
    

class lista_enlazada:
    def __init__(self):
        self.head=None
        self.size=0
    
        """Funcion: Insertar_nodo_inicio
        Se le asigna a siguiente head y  se inserta el nuevo valor al inicio de la lista.
        """
    def insertar_nodo_inicio(self,valor):
        nuevo_nodo=Nodo(valor)
        nuevo_nodo.siguiente= self.head
        self.head=nuevo_nodo
        self.size+=1
        del nuevo_nodo
        
        """Funcion: Insertar_nodo_final
        Primero se busca si esta vacio o no,si esta vacio crea el nuevo Nodo.
        Si ya contiene otros nodos se crea un temporal que va a recorrer la lista hasta que llegue al ultimo nodo e inserta el ultimo valor. 
        """
    def insertar_nodo_final(self,valor):

        nuevo_nodo=Nodo(valor)

        if self.head is None:
            self.head = nuevo_nodo
            return

        temporal= self.head

        while (temporal.siguiente != None):
            temporal=temporal.siguiente
            self.size+=1
        temporal.siguiente=nuevo_nodo

    """Funcion: imprimir_lista
    Si el nodo a imprimir es igual a NULL entonces la lista esta Vacia
    De lo contrario va a imprimir el valor, y continuar con el siguiente, hasta que siguiente sea igual a NULL.
    """
    def imprimir_lista(self):          
          

      if (self.head == None):  
         print("Lista Vacia")
         return

      else:
            temporal=self.head
            while (temporal != None):
                print(temporal.valor)
                temporal=temporal.siguiente

    """Funcion: getSize
    Esta funcion obtiene la cantidad de nodos que contiene la lista
    """
    def getSize(self):
        return self.size


    """Funcion: buscar
    Se crea un temporal y un contador y pregunta si el indice es menor al tamano de la lista o indice es menor a 0
    Mientras el contador de diferente del indice va a recorrer la lista e incrementar en uno el contador, cuando sean iguales retorna el valor que se encuentra en esa posicion de nodo
    """
    def buscar(self, indice):

        temp = self.head
        contador = 0

        if indice < self.size or indice < 0:
          while(contador != indice):

            temp = temp.siguiente
            contador +=1

            return temp

    
    """Funcion: borrar
    Esta borra uno por uno de los nodos de la lista
    """
    def borrar(self):
        temporal = self.head
        if (temporal == None):
           print("\n Lista vacia ")
        while temporal:
           self.head = temporal.siguiente
           temporal = None
           temporal = self.head

    """Funcion: reverse
    Se asigna el valor de referencia del nodo actual al siguiente,
    luego se establece el valor de referencia del nodo actual al temporal anterior, se establece anterior al nodo actual de temporal, y se establce el nodo actual de temporal al valor siguiente del nodo 
    """
    def reverse(self):
        anterior = None
        temporal = self.head
        while (temporal != None):
            next = temporal.siguiente
            temporal.siguiente = anterior
            anterior = temporal
            temporal = next
        self.head = anterior

    """Funcion: Main
    Se hace una llamada a todas las funciones necesarias para el correcto funcionamiento  
    """
def main():
      print("Lista agregar inicio:")
      miLista=lista_enlazada()
      miLista.insertar_nodo_inicio(1)
      miLista.insertar_nodo_inicio(2)
      miLista.insertar_nodo_inicio(3)
      miLista.insertar_nodo_inicio(4)
      miLista.insertar_nodo_inicio(5)
      miLista.imprimir_lista()


      print("\nCantidad:")
      print(miLista.getSize())

      print("\nEl numero en la posicion 1 es: ")
      objeto_indice = miLista.buscar(1)
      print(objeto_indice.valor)

      print("\nLista invertida:")
      miLista.reverse()
      miLista.imprimir_lista()

      print("\nLista eliminada:")
      miLista.borrar()
      miLista.imprimir_lista()

      print("\nLista agregar final:")
      miLista=lista_enlazada()
      miLista.insertar_nodo_inicio(1)
      miLista.insertar_nodo_inicio(2)
      miLista.insertar_nodo_inicio(3)
      miLista.insertar_nodo_inicio(4)
      miLista.insertar_nodo_inicio(5)
      miLista.imprimir_lista()
      return

print(main())
