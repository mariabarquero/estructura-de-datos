/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante:María José Barquero Pérez/2019037947
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	nodo_estudiante *ref_fin;     //Se agrega un puntero a final
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parámetros
	Salidas: No retorna nada 
	Funcionamiento: 
	     - Se le asigna espacio de memoria a ref_lista
             - Se le asigna al nodo cantidad un valor (0)
             - El nodo ref_inicio apunta a NULL   
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Puntero nuevo de tipo nodo_estudiante
	Salidas:  No retorna nada
	Funcionamiento: 
	     - Si lista es nulo se va a inicializar la lista
	     - Cuando ref_lista no sea nula al puntero siguiente se le asigna ref_inicio
	     - A ref_inicio se le asigna el nuevo nodo 
	     - Nodo cantidad aumenta en 1
	     - Insertando el nuevo nodo en la lista al inicio
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Puntero nuevo de tipo nodo_estudiante
	Salidas: No retorna nada
	Funcionamiento:
	     - Si lista es nulo se va a inicializar la lista
	     - Cuando cantidad sea igual a 0 va a insertar nodo al inicio de la lista
	     - Cuando cantidad no sea 0, se crea un temporal que se le asigna espacio en memoria
	     - Temporal va a ser igual a ref_inicio
	     - Mientras ref_siguente sea diferente a nulo, va a ir recorriendo la lista
	     - Cuando ref_siguiente sea igual a nulo, se le asigna a ref_siguiente el nuevo nodo
	     - Cantidad aumenta en 1
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Un indice de tipo entero 
	Salidas: No retorna nada
	Funcionamiento: 
	     - Se crea un temporal, que se le asigna espacio de memoria 
	     - Temporal va a ser igual a NULL
	     - Se crea cont de tipo entero, se le asigna valor 0
	     - Cuando ref_inicio es igual a NULL imprime mensaje de lista esta vacia
	     - Cuando indice es mayor o igual a cantidad imprime en pantalla el indice ingresado no es valido
	     - Cuando indice sea menor a cantidad se crea otro temporal, se reserva espacio de memoria y se le asigna ref_incio 
	     - Cuando indice es igual a 0  ref_inicio se le asigna el temporal siguiente y cantidad disminuye en 1
	     - Se libera temporal en la memoria con free 
	     - Cuando ref_siguente sea diferente de nulo cont va a aumentar en 1
	     - Cuando cont sea igual a indice, temporal siguiente va a ser igual al puntero de temporal siguiente siguiente
	     - cantidad disminuye en 1
	     - temporal va a ser igual a temporal siguiente 
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: indice de tipo entero
	Salidas: retorna temporal 
	Funcionamiento: 
	     - Se crea un temporal,se asigna espacio en memoria , temporal apunta a NULL
	     - Cuando ref_inicio sea igual a NULL o cantidad sea igual a 0 va a imprimir la lista esta vacia y retornar temporal 
	     - Si no se cumple esa condicion se va a crear un cont entero en 0, un nuevo temporal que se le asigna espacio en memoria y apunta a ref_icinio
	     - Mientras ref_siguiente sea diferente de NULL y cont sea igual a indice va a retornar nuevo temporal
	     - Cuando cont sea diferente de indice va a recorrer la lista y cont aumenta en 1
	     - retorna temporal con el elemento que se estaba buscando 
	    
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: carnet_almacenado y carnet_ingresado de tipo entero
	Salidas:  no retorna nada
	Funcionamiento:  
	     - Cuando carnet_almacenado y carnet_ingresado son iguales, imprimir carnet ingresado correcto
	     - Cuando sean diferentes imprime en pantalla el carnet ingresado es diferente
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No tiene entradas
	Salidas:  No retorna nada
	Funcionamiento: 
	     - Crea opcion,indice,carnet de tipo entero
	     - Crea un temporal de tipo nodo_estudiante
	     - Inicializa la funcion imprimir, y muestra un menu con las diferentes opciones 
	     - Se le asigna a max_size 1 y a opcion la opcion que digito el usuario
	     - Por medio de un switch segun la opcion de entrada, va iniciar los diferetnes casos
	     - Si opcion igual a 0 se sale del programa 
	     - Si opcion igual a 1, asigna a temporal la funcion de crear nodo, y llama a la funcion insertar_final con parametro temporal 
	     - Si opcion igual a 2, asigna a temporal la funcion de crear nodo, y llama a la funcion insertar_inicio con parametro temporal
	     - Si opcion igual a 3, pregunta a usuario carnet que desea validar, se le asigna a indice 1 y a temporal la funcion             buscar_por_indice con paramentro de indice
	     - Cuando temporal sea diferente de nulo pregunta el carnet de estudiante que desea validar, se le asigna a carnet la respuesta    ingresada por el usuario, y se llama a la funcion validar_carnets con parametro carnet, y carnet de la lista
             - Si opcion igual a 4, pregunta a usuario posicion de carnet que desea borrar, asigna a indice la opcion del usuario
               y llama a la funcion borrar_por_indice que lleva como parametro indice 
         
	     
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No tiene entradas
	Salidas:  Retorna 0
	Funcionamiento: 
	     - Inicializa la funcion de menu
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: max_size
	Salidas:  retorna el buffer
	Funcionamiento: 
	    - Crea un puntero buffer de tipo char 
	    - Se le asigna espacio de memoria a buffer
	    - Cuando buffer sea igual a NULL, imprimir no fue posible reservar memoria para el buffer y se sale del programa 
	    - Si buffer es diferente de NULL se le asigna a character que recive de parametros la opcion del usuario,el tama;o y stdin para indicear que es de entrada
	    - Se le asigna un 0 a la ultima posicion-1 
	    - retorna buffer con la opcion que ingreso el usuario
	    
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
        //size_t: puede alamacenar el tamaño máximo de un objeto teóricamente posible de cualquier tipo
	Entradas: max_size
	Salidas:  retorna numerical_input
	Funcionamiento: 
	     - Se le asigna a numerical_input la cadena convertida en int de la funcion get_user_input
	     - Retorna entero de la funcion get_user_input 
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
