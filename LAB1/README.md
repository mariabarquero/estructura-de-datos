IC-2011 Estructuras de Datos
Prof: MSc. Samanta Ramijan Carmiol

Laboratorio 1
Arreglos y Structs.

En clase hemos estudiado los conceptos básicos de struct y arreglo, para poner en práctica
los conocimientos se le solicita a las y los estudiantes que escriban un programa en C, que
permita al usuario llenar los datos de 10 estudiantes, en un arreglo de tamaño 10, de tipo
struct estudiante. El programa deberá entonces, preguntar por el carnet y el nombre
de 10 estudiantes (realice lo anterior con ayuda de un ciclo). Una vez almacenados todos los
datos en la estructura, el programa tendrá el siguiente comportamiento:
¿Qué posición de carnet desea validar?
2
¿Cuál es el carnet del estudiante en la posición 2?
201925317
El carnet ingresado el correcto
201928617
El carnet ingresado no corresponde con la posición 2.

Carne:
201925319
Nombre:
Matías

Carne:
201925318
Nombre:
María

Carne:
201925317
Nombre:
Jorge

Carne:
201925316
Nombre:
Sofía

Aspectos Administrativos
1. Límite para la entrega de la asignación: Lunes 5 de agosto a las 8am.
2. Plataforma de revisión: Repositorio de código git
3. Cada archivo debe estar debidamente documentado con la información personal del
estudiante que lo escriba, además de explicar su código e indicar cualquier
referencia a código de terceros
4. Se debe incluir un archivo README que contenga el enunciado de los ejercicios.
